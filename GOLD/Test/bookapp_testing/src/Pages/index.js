import SplashScreen from "./SplashScreen";
import WelcomeAuth from "./WelcomeAuth";
import Login from "./Login";
import Register from "./Register";
import HomeScreen from "./HomeScreen";
import RegisterSuccess from "./RegisterSuccess";
import Detail from "./Detail/index";
import VideoScreen from "./VideoScreen";
import PdfScreen from "./PdfScreen";

export {SplashScreen, WelcomeAuth, Login, Register, HomeScreen, RegisterSuccess, Detail, VideoScreen, PdfScreen}