
const checkTypeNumber = (givenNumber) => {
  
  if (!givenNumber) {
    return 'Error: Bro where is the parameter?'
    } else {
      if (typeof givenNumber !== 'number') {
        return 'Error : Invalid type data ';
      } else if (givenNumber % 2 === 0) {
        return 'GENAP';
      } else if (givenNumber % 2 === 1) {
        return 'GANJIL';
      }
    }
}

console.log(checkTypeNumber(10));
console.log(checkTypeNumber(3));
console.log(checkTypeNumber("10"));
console.log(checkTypeNumber({}));
console.log(checkTypeNumber([]));
console.log(checkTypeNumber());
